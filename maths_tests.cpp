#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "maths.h"

TEST_CASE("test primes", "[is_prime]")
{
  REQUIRE(is_prime(2) == true);
  REQUIRE(is_prime(3) == true);
  REQUIRE(is_prime(4) == false);
  REQUIRE(is_prime(5) == true);
  REQUIRE(is_prime(6) == false);
  REQUIRE(is_prime(7) == true);
  REQUIRE(is_prime(8) == false);
  REQUIRE(is_prime(9) == false);
}

TEST_CASE("test absolutes", "[absolute]")
{
  REQUIRE(absolute(1) == 1);
  REQUIRE(absolute(200000) == 200000);
  REQUIRE(absolute(-200000) == 200000);
  REQUIRE(absolute(-3.14513281928) == 3.14513281928);
  REQUIRE(absolute(0) == 0);
}

TEST_CASE("test powers", "[power]")
{
  // normal cases
  REQUIRE(power(1, 1) == 1);
  REQUIRE(power(2, 3) == 8);
  REQUIRE(power(4, 4) == 256);

  // anything raised to 0 is 1
  REQUIRE(power(0, 0) == 1);
  REQUIRE(power(14, 0) == 1);

  // handle negative exponents: x⁻ⁿ as 1/xⁿ
  REQUIRE(power(2, -3) == 0.125);
}
