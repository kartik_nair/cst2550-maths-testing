#include "maths.h"

double absolute(double number)
{
  if (number < 0)
    number = -number;
  return number;
}

double power(double number, int exponent)
{
  // anything raised to 0 is 1
  if (exponent == 0) return 1;

  // negative exponent
  bool negative_exponent = exponent < 0;
  if (negative_exponent) exponent = -exponent;

  double result = number;
  for (int i = 0; i < exponent - 1; ++i)
    result *= number;

  return negative_exponent ? 1 / result : result;
}

bool is_prime(int number)
{
  for (int i = 2; i <= number / 2; ++i)
    if (number % i == 0)
      return false;
  return true;
}

